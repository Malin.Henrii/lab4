package datastructure;

import java.awt.Color;

import cellular.CellState;

public class CellGrid implements IGrid {
	
	
	private int rows;
	private int columns;
	private CellState[][] cellState;

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
		this.columns = columns;
		cellState = new CellState[rows][columns];
		
		for(int row=0; row < rows; row++) {
			for(int col=0; col < columns; col++) {
				cellState[row][col]= initialState;
			}
		}
		
		
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
      
    	cellState[row][column]= element;
        
    }

    @Override
    public CellState get(int row, int column) {
        
    	Color cellColor = cellState[row][column].getColor();
    	
    	if( cellColor == Color.black) {
    		return CellState.ALIVE;
    	}else if (cellColor == Color.red) {
    		return CellState.DYING;
    	}
    	
        return CellState.DEAD;
    }

    @Override
    public IGrid copy() {
    	
    	IGrid copyGrid = new CellGrid(rows, columns, CellState.DEAD);
		for(int row=0; row < rows; row++) {
			for(int col=0; col < columns; col++) {
				copyGrid.set(row, col, this.get(row, col));
			}
    
    }
	return copyGrid;
    
}
}
