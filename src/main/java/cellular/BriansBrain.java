package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {


		
		
		IGrid currentGeneration;


		public BriansBrain(int rows, int columns) {
			currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
			initializeCells();
		}

		@Override
		public void initializeCells() {
			Random random = new Random();
			for (int row = 0; row < currentGeneration.numRows(); row++) {
				for (int col = 0; col < currentGeneration.numColumns(); col++) {
					if (random.nextBoolean()) {
						currentGeneration.set(row, col, CellState.ALIVE);
					} else {
						currentGeneration.set(row, col, CellState.DEAD);
					}
				}
			}
		}

		@Override
		public int numberOfRows() {
			return currentGeneration.numRows();
		}

		@Override
		public int numberOfColumns() {
			return currentGeneration.numColumns();
		}

		@Override
		public CellState getCellState(int row, int col) {
		
			return currentGeneration.get(row, col);
		}

		@Override
		public void step() {
			IGrid nextGeneration = currentGeneration.copy();
			
			for(int row=0; row < numberOfRows(); row++) {
				for(int col=0; col < numberOfColumns(); col++) {
					nextGeneration.set(row, col, getNextCell(row, col));
					
					}
				}
		}

		@Override
		public CellState getNextCell(int row, int col) {
			
			CellState stateOfCell = getCellState(row,col);
		
			int aliveNeighbors = countNeighbors(row,col, CellState.ALIVE);
			
			if(stateOfCell.equals(CellState.ALIVE)){
				return CellState.DYING;
			}else if(stateOfCell.equals(CellState.DYING)) {
				return CellState.DEAD;
			}else if(stateOfCell.equals(CellState.DEAD) && (aliveNeighbors == 2)) {
				return CellState.ALIVE;
			}
				
			
			return CellState.DEAD;
		}

		private int countNeighbors(int row, int col, CellState state) {
			
			int countStateAlive = 0;
			
			for(int i = row - 1; i <= row + 1; i++) {
				for(int j = col - 1; j<= col + 1; j++) {
					if((i>= currentGeneration.numRows()|| (j>= currentGeneration.numColumns())|| (i<0)|| (j<0))) {
						continue;
					}
					if ((i==row)&& (j == col)) {
						continue;
					}
					else if (state == currentGeneration.get(i, j)) {
						countStateAlive++;
					}
				}
			}
			
			return countStateAlive;
		}

		@Override
		public IGrid getGrid() {
			return currentGeneration;
		}
	}
		
	


